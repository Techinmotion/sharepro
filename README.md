# How to Promote Your Music in 2012 #

A couple of decades ago it was unlikely an aspiring musician would get the opportunity to record their own music. If you were in a band and wanted to record a proper sounding album you would need to be signed to a record label which would pay for the advance for you to make an album. The advances in digital recording in the last decade have it made it possible for musicians with even a modest budget and proper skills to make something that may be worth listening to. Although, it's great that musicians now have the ability the record in the privacy of their own homes it has saturated the market with an excess of music on the internet that would take endless hours to sift through. Luckily, if you have recorded something worth listening to their maybe a couple of ways you can rise above the masses and get your music heard.

1.Streaming music services

This day and age more and more people are subscribing to different online streaming music services like Spotify, MOG, and Pandora. A lot of musicians might not know that submitting your music to these online services is usually a very straight forward process. For example, in order to submit your music to Spotify you would have to license your music through an artist aggregator like CD baby or to Ditto. Services like this also make it possible to submit to other digital services like Itunes or Amazon at the same time. The process for submitting your music can vary for each streaming services but from what I have researched I haven't found one that is to hard to accomplish.

2.Social Media

If you haven't already it is integral that you join as many social media outlets as possible. Everyone knows about Facebook and Twitter but there are other sources as well like Pinterest, Bebo, Netlog that could be a valuable way to promote your music. There are lots of interesting ways you can market your music through social media.Think outside of the box so people can get interested and involved with your music. For example, bring a video camera in the recording sessions of your band making a new album, then post the videos on YouTube and then go on Facebook to tell all your friends about it.

3. Radio Stations

Don't forget about the power of radio stations. The good news is that are a lot of small independent radio stations on the internet that you can submit your music to. Services like shoutcast and live365 have thousands of radio stations that you can try to submit your music to. You obviously want to submit to stations that cater to your genre and have a decent amount of listeners. Sirius radio has over 20 million subscribers so make sure you submit your music to them.

4. Share Pro

If you haven't heard of Share Pro yet you need to start taking advantages of these services. Share Pro is a service that dedicated to letting people distribute and listen to sound files. It is unique in that it lets artists have their own distinctive URL which they can then embed on their own website. It is also very easy to integrate with Facebook and Twitter. If you are a paid member you are granted more hosting space and can distribute to more groups.

5.Get reviewed

Hopefully, other people will appreciate your music as much as you do. That's why it important to submit your music to different magazines and websites.A good review on a popular website like "pitchfork" can catapult a band from playing local bars to being on a national tour. Tiny mix tapes, drowned in sound, and the a.v. club are just a couple of places you should be submitting your music to.

These are only a couple things you can do to get your music heard. There is a lot of competition out there but if you make quality music and use the methods that are at your disposal you are bound to get heard


[https://www.sharetopros.com](https://www.sharetopros.com)

